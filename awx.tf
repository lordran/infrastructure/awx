provider "hcloud" { }

resource "hcloud_server" "awx" {
    name        = "awx-${var.awx_id}"
    image       = "ubuntu-18.04"
    server_type = "${var.awx_instance_type}"
    location    = "nbg1"
    ssh_keys    = ["johann@sif.lordran.net", "johann@online"]
    labels      = {"project" = "awx"}
}

output "awx_public_ip" {
    value = "${hcloud_server.awx.ipv4_address} - ${hcloud_server.awx.ipv6_address}"
}
