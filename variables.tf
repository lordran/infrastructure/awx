variable "awx_id" {
    default = "prod"
}

variable "awx_instance_type" {
    default = "cx21-ceph"
}
